let http = require ("http");

//require() is a built in JS method which allows us to import packages
	//packages are pieces of code we can integrate into our application.

//http is a default page that comes from NodeJS.  It also allows us to use methods that allows us to create SERVERS.

//http is a module. Modules are packages we imported
//modules are objects that contain codes, methods, or data.

//protocol to client-server communication - http://localhost: 4000 - server/applications

http.createServer(function(request, response){

	/*
		createServer() method - is a method from the httpo module that allows us to handle requests comming from the client and a server respectively

		the request object contains details of the request fromt eh client

		the reponse object contains details for the repsonse of the server.

		the createServe() method ALWAYS receives the request object first before the reponse

	*/

	/*
		response.writeHead() - is a method of the respon se object.  It allows us to add:

		Headers - headers are additional info about our response.
		We have 2 argument in our writeHead() method: (1) HTTP status code

		HTTP status is just a numerical code that let the client know about the status of their request

		200 means ok
		404 means the resource cannot be found

		(2) "Content-Type" is one of the recognizable headers.  It simply pertains to the data type of our response.

	*/

	/*
		Response.end() - end our response.  It is also able to send a message/data as a STRING



	*/

	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end("Hello, Marv!");

}).listen(5000);


	/*
		.listen() allows us to assign a port to our server
		This will allow us serve our file (sample.js) server in our local machine assigned to port 5000

		There are several tasks and process on our computer that run on different port numbers

		hypertext transfer protocol - http://localhost:5000/ server

		localhost: ---- Local Machine:  5000 - is the current assigned to our server

		4000, 4040, 8000, 5000, 3000, 4200 - usually used for webdevelopment



	*/

console.log("Server is running on localhost:5000!");

//we use console.log to show validation from which port number our server is running on.