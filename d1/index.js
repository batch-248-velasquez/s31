/*
	What is a client?

	A clien is an application which creates request for resources from a server.  A client will rigger an action, in the web development context, through a URL and wait for the response of the server


	What is a server?

	A server is able to host and deliver RESOURCES  requersted by a client.  In fact, a single server can handle multiple clients.

	What is Node.js?

	Node.js is a runtime environment which allows us to create or develop backend/server-side applications with JavaScript.  Becuase by default, JS was conceptualized solely to the front end.
	Runtime Environment - is the environment in which the program or application is executed

	Why is Node.js popular?

	Performance - nodeJS is one of the most performing environment for creating backend application for JavaScript

	Familiarity - Since NodeJS is built and uses JS as its language, it is very familiar for most develpers

	NPM -Node Package Manager - is the largest registry for node packages
	Packages- are bits of programs, methods, functions, codes that greatly help in the development of the application.


*/

const http = require("http");

//creates a variable "port" to store the port number
const port = 4000

const server = http.createServer((request,response)=>{

	if (request.url == '/greeting'){

		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end(`Welcome to 248 2048 App`)


	}

	else if (request.url == '/homepage'){

		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end(`This is the homepage!`)
	}

	else {

		response.writeHead(404,{'Content-Type':'text/plain'})
		response.end(`404 Page not available!`)
	}

})

server.listen(port)

console.log(`Server is now accessible at localhost:${port}`)